﻿
var menuState = {
    preload: function () {
        game.load.image('background', 'assets/gameBackGround.png');

        game.load.audio('backgroundSound', 'assets/m_starwars.ogg');
    },
    create: function () {

        requestScoreBoard('single-player');

        game.stage.backgroundColor = '#fff';
        game.add.image(-50, -50, 'background');

        if (game.backgroundVolume && game.backgroundVolume.context.state === 'suspend') game.backgroundVolume.resume();
        game.backgroundVolume = game.add.audio('backgroundSound');
        game.backgroundVolume.loop = true;
        game.backgroundVolume.play();

        this.cursor = game.input.keyboard;

        const font_style = {
            font: '4rem "Lucida Console", Monaco, monospace',
            fill: '#353b48',
            align: 'center',
            boundsAlignH: 'center'
        }
        this.title = game.add.text(0, game.height / 4, 'Cell War', { ...font_style, font: '6rem "Lucida Console", Monaco, monospace', fontWeight: 'bold' });
        this.title.setTextBounds(0, 0, game.width, 50);

        this.font_box = game.add.group();
        this.startLabel = game.add.text(0, game.height / 2, 'GO !', { ...font_style, backgroundColor: '#e84118', fill: 'white' }, this.font_box);
        this.exitLabel = game.add.text(0, game.height / 2 + 70, 'EXIT', { ...font_style }, this.font_box);
        this.font_box.forEach(font => {
            font.setTextBounds(0, 0, game.width, 50);
        });

        this.list = this.font_box.length;
        this.activeItem = 0;
    },
    update: function () {

        if (this.cursor.justPressed(Phaser.Keyboard.DOWN)) {
            this.blur(this.font_box.children[this.activeItem]);
            this.activeItem = (this.activeItem + this.font_box.length + 1) % this.font_box.length;
            this.focus(this.font_box.children[this.activeItem]);
        }
        else if (this.cursor.justPressed(Phaser.Keyboard.UP)) {
            this.blur(this.font_box.children[this.activeItem]);
            this.activeItem = (this.activeItem + this.font_box.length - 1) % this.font_box.length;
            this.focus(this.font_box.children[this.activeItem]);
        }
        else if (this.cursor.justPressed(Phaser.Keyboard.ENTER)) {
            switch (this.font_box.children[this.activeItem]._text) {
                case 'GO !': game.state.start('setting'); break;
                case 'EXIT': game.state.start('menu'); break;
                default: break;
            }
        }
    },
    focus: function (text) {
        text.setStyle({
            font: '4rem "Lucida Console", Monaco, monospace',
            align: 'center',
            boundsAlignH: 'center',
            backgroundColor: '#e84118',
            fill: 'white'
        });
    },
    blur: function (text) {
        text.setStyle({
            font: '4rem "Lucida Console", Monaco, monospace',
            align: 'center',
            boundsAlignH: 'center',
            backgroundColor: 'transparent',
            fill: '#353b48'
        });
    }
}
var gameOverState = {
    preload: function () {
        game.load.image('background', 'assets/gameBackGround.png');
    },
    create: function () {
        game.stage.backgroundColor = '#fff';
        game.add.image(-50, -50, 'background');

        this.cursor = game.input.keyboard;

        const font_style = {
            font: '4rem "Lucida Console", Monaco, monospace',
            fill: '#353b48',
            align: 'center',
            boundsAlignH: 'center'
        }
        this.title = game.add.text(0, game.height / 4, `${mainState.win? 'YOU WIN!' : 'GAME OVER'}`, { ...font_style, font: '6rem "Lucida Console", Monaco, monospace', fontWeight: 'bold' });
        this.title.setTextBounds(0, 0, game.width, 50);

        this.font_box = game.add.group();
        this.reset = game.add.text(0, game.height / 2, 'RESTART', { ...font_style, backgroundColor: '#e84118', fill: 'white' }, this.font_box);
        this.exit = game.add.text(0, game.height / 2 + 70, 'EXIT', { ...font_style }, this.font_box);
        this.font_box.forEach(font => {
            font.setTextBounds(0, 0, game.width, 50);
        });

        this.list = this.font_box.length;
        this.activeItem = 0;
    },
    update: function () {

        if (this.cursor.justPressed(Phaser.Keyboard.DOWN)) {
            this.blur(this.font_box.children[this.activeItem]);
            this.activeItem = (this.activeItem + this.font_box.length + 1) % this.font_box.length;
            this.focus(this.font_box.children[this.activeItem]);
        }
        else if (this.cursor.justPressed(Phaser.Keyboard.UP)) {
            this.blur(this.font_box.children[this.activeItem]);
            this.activeItem = (this.activeItem + this.font_box.length - 1) % this.font_box.length;
            this.focus(this.font_box.children[this.activeItem]);
        }
        else if (this.cursor.justPressed(Phaser.Keyboard.ENTER)) {
            switch (this.font_box.children[this.activeItem]._text) {
                case 'RESTART': postScore(function () { game.state.start('main'); }); break;
                case 'EXIT': postScore(function () { game.state.start('menu'); }); break;
                default: break;
            }
        }

        document.querySelector('button').onclick = function () {
            document.querySelector('#form input').value = '';
            document.querySelector('#msk').style.display = 'none';
            game.state.start('menu');
        };
    },
    focus: function (text) {
        text.setStyle({
            font: '4rem "Lucida Console", Monaco, monospace',
            align: 'center',
            boundsAlignH: 'center',
            backgroundColor: '#e84118',
            fill: 'white'
        });
    },
    blur: function (text) {
        text.setStyle({
            font: '4rem "Lucida Console", Monaco, monospace',
            align: 'center',
            boundsAlignH: 'center',
            backgroundColor: 'transparent',
            fill: '#353b48'
        });
    }
}
var settingState = {
    preload: function () {
        game.load.image('background', 'assets/gameBackGround.png');
    },
    create: function () {
        game.stage.backgroundColor = '#fff';
        game.add.image(-50, -50, 'background');

        this.cursor = game.input.keyboard;

        const font_style = {
            font: '4rem "Lucida Console", Monaco, monospace',
            fill: '#353b48',
            align: 'center',
            boundsAlignH: 'center'
        }
        this.title = game.add.text(0, game.height / 4, 'SETTING', { ...font_style, font: '6rem "Lucida Console", Monaco, monospace', fontWeight: 'bold' });
        this.title.setTextBounds(0, 0, game.width, 50);

        this.font_box = game.add.group();
        this.volumeLabel = game.add.text(0, game.height / 2, `VOLUME ${this.volume}`, { ...font_style, backgroundColor: '#e84118', fill: 'white' }, this.font_box);
        this.modeLabel = game.add.text(0, game.height / 2 + 70, 'MODE', { ...font_style }, this.font_box);
        this.startLabel = game.add.text(0, game.height / 2 + 140, 'START', { ...font_style, }, this.font_box);
        this.exitLabel = game.add.text(0, game.height / 2 + 210, 'EXIT', { ...font_style }, this.font_box);
        this.font_box.forEach(font => {
            font.setTextBounds(0, 0, game.width, 50);
        });

        this.list = this.font_box.length;
        this.activeItem = 0;

        this.volume = 50;
    },
    update: function () {

        if (this.cursor.justPressed(Phaser.Keyboard.DOWN)) {
            this.blur(this.font_box.children[this.activeItem]);
            this.activeItem = (this.activeItem + this.font_box.length + 1) % this.font_box.length;
            this.focus(this.font_box.children[this.activeItem]);
        }
        else if (this.cursor.justPressed(Phaser.Keyboard.UP)) {
            this.blur(this.font_box.children[this.activeItem]);
            this.activeItem = (this.activeItem + this.font_box.length - 1) % this.font_box.length;
            this.focus(this.font_box.children[this.activeItem]);
        }
        if (this.cursor.justPressed(Phaser.Keyboard.LEFT)) {
            this.volume = Math.max((this.volume-1), 0);
            game.backgroundVolume.volume = this.volume/100;
        }
        else if (this.cursor.justPressed(Phaser.Keyboard.RIGHT)) {
            this.volume = Math.min(100, (this.volume+1), 100);
            game.backgroundVolume.volume = this.volume/100;
        }
        else if (this.cursor.justPressed(Phaser.Keyboard.ENTER)) {

            //game.sound.volume = this.volume;

            switch (this.font_box.children[this.activeItem]._text) {
                case 'START': game.state.start('main'); break;
                case 'EXIT': game.state.start('menu'); break;
                default: break;
            }
        }
        this.volumeLabel.text = `VOLUME ${this.volume}`;
    },
    focus: function (text) {
        text.setStyle({
            font: '4rem "Lucida Console", Monaco, monospace',
            align: 'center',
            boundsAlignH: 'center',
            backgroundColor: '#e84118',
            fill: 'white'
        });
    },
    blur: function (text) {
        text.setStyle({
            font: '4rem "Lucida Console", Monaco, monospace',
            align: 'center',
            boundsAlignH: 'center',
            backgroundColor: 'transparent',
            fill: '#353b48'
        });
    }
}