﻿class Element {
    constructor(obj) {
        this.element = game.add.sprite(obj.x || 0, obj.y || 0, obj.type);
        this.element.anchor.setTo(0.5, 0.5);
        this.r = 10;
        this.type = obj.type || 'medical';
        this.power = obj.power || 10;
        this.velocity = obj.velocity || 0.5;
        this.eater = obj.eater;

        game.physics.arcade.enable(this.element);
    }
    effect(_target) {
        var target = _target['_prnt'];
        switch (this.type) {
            case 'medical': target.lifeValCal = this.power; break;
            case 'anti-weapon':
                target.enableAntiWeapon = true;
                target.antiWeaponCnt = 200;
                target.MaxBulletCount = Math.max(target.MaxBulletCount, Infinity);
                break;
            case 'heavy-mechine':
                target.enableHeavyMechine = true;
                target.MaxBulletCount = Math.max(target.MaxBulletCount, 10);
                target.heavyMachineCnt = 50;
                break;
            default: break;
        }
    }
    moveElement() {
        if (this.element.y > game.height + 10) return true;

        this.element.y += this.velocity;

        var hover = false;
        this.eater.forEach(cell => {
            if (!cell.sprite['_prnt']) cell.sprite['_prnt'] = cell;
            if (game.physics.arcade.overlap(cell.sprite, this.element, this.effect, null, this)) hover = true;
        });

        return hover;
    }
    remove() {
        while (this.eater.length) this.eater.pop();
        this.element.destroy();
        this.element = null;
    }
}