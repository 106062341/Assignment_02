﻿function getLevel() {
    return [
        {
            _level: [
                {
                    enmB: [
                        { x: 60, y: 10, left: 0, right: game.width, v: 2 },
                        { x: 120, y: 70, left: 0, right: game.width, v: 2 },
                        { x: 180, y: 140, left: 0, right: game.width, v: 2 }
                    ]
                },
                {
                    enmB: [
                        { x: 60, y: 10, left: 0, right: game.height, v: 2, isVertical: true },
                        { x: 120, y: 70, left: 0, right: game.height, v: 2, isVertical: true },
                        { x: 180, y: 140, left: 0, right: game.height, v: 2, isVertical: true }
                    ]
                },
                {
                    enmA: [{ x: 60, y: -100 }, { x: 120, y: -100 }]
                }
            ]
        },
        {
            _level: [
                {
                    enmB: [
                        { x: 60, y: 10, left: 0, right: game.height, v: 2, isVertical: true },
                        { x: 120, y: 70, left: 0, right: game.height, v: 2, isVertical: true },
                        { x: 180, y: 140, left: 0, right: game.height, v: 2, isVertical: true },
                        { x: 240, y: 210, left: 0, right: game.height, v: 2, isVertical: true },
                        { x: 300, y: 280, left: 0, right: game.height, v: 2, isVertical: true }
                    ]
                },
                {
                    enmA: [{ x: 60, y: -100 }, { x: 120, y: -100 }, { x: 240, y: -100 }],
                    enmB: [
                        { x: 60, y: 10, left: 0, right: game.width, v: 2 },
                        { x: 120, y: 70, left: 0, right: game.width, v: 2 },
                        { x: 180, y: 140, left: 0, right: game.width, v: 2 },
                        { x: 240, y: 210, left: 0, right: game.width, v: 2 },
                        { x: 300, y: 280, left: 0, right: game.width, v: 2 }
                    ]
                },
                {
                    enmA: [{ x: 60, y: -100 }, { x: 120, y: -100 }, { x: 180, y: -100 }],
                    enmB: [
                        { x: 60, y: 10, left: 0, right: game.width, v: 2 },
                        { x: 120, y: 70, left: 0, right: game.width, v: 2 },
                        { x: 180, y: 140, left: 0, right: game.width, v: 2 },
                        { x: 240, y: 210, left: 0, right: game.width, v: 2 },
                        { x: 300, y: 280, left: 0, right: game.width, v: 2 }
                    ]
                }
            ]
        },
        {
            _level: [
                {
                    enmA: [{ x: 60, y: -100 }, { x: 120, y: -100 }, { x: 180, y: -100 }, { x: 240, y: -100 }]
                },
                {
                    enmA: [{ x: 60, y: -100 }, { x: 120, y: -100 }, { x: 180, y: -100 }, { x: 240, y: -100 }, { x: 180, y: -100 }, { x: 240, y: -100 }],
                },
                {
                    enmA: [{ x: 60, y: -100 }, { x: 120, y: -100 }, { x: 180, y: -100 }],
                    enmC: [{ x: game.width / 2, y: 0, v: 12 }]
                }
            ]
        }
    ];
}