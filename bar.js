﻿class Bar {
    constructor(obj) {
        this.bar = game.add.sprite(obj.x + 120 || 120, obj.y || 0, obj.barName);
        this.x = obj.x || 0;
        this.y = obj.y || 0;
        this.val = obj.val || 0;
        this._val = obj._val || 100;
        this.enableScale = obj.enableScale;
        this.text = obj.barText;
        this.textLabel = (obj.barText) ?
            game.add.text(obj.x, obj.y, obj.barText + ' ' + obj.val, { font: '14px "Lucida Console", Monaco, monospace', fill: '#2f3640' }) : null;

        game.add.tween(this.bar.scale).to({ x: Math.min(this.val / this._val, 1), y: 1 }, 100).start();
    }
    set valCal(num) {
        if (num != this.val) {
            game.add.tween(this.bar.scale).to({ x: Math.min(num / this._val, 1), y: 1 }, 100).start();
            if (this.textLabel) this.textLabel.text = this.text + ' ' + num;
            if (this.enableScale) game.add.tween(this.textLabel.scale).to({ x: 1.5, y: 1.5, angle: 10 }, 100).yoyo(true).start();
            this.val = num;
        }
    }
}