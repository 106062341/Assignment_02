class Player {
    constructor(obj) {
        // self
        this.player = game.add.sprite(obj.x - obj.r, obj.y - obj.r, 'player');
        this.player.animations.add('walk', [1, 2, 3, 4, 5, 6], 16, true);

        this.cursor = game.input.keyboard.createCursorKeys();
        this.r = obj.r || 50;
        this.lifeVal = obj.r * 2 || 100;
        this._life_val_ = obj.r * 2 || 100;
        this.activeVal = 100;
        this.pos = {
            x: obj.x || 0,
            y: obj.y || 0
        };
        this.attackRange = 100;
        this.detectRange = 800;
        this.velocity = 100;
        this.power = 5;
        this.score = 0;
        this.MaxBulletCount = 3;
        this.bulletSet = [];
        game.physics.arcade.enable(this.player);
        if (!this.player.inWorld) { game.state.start('main'); }

        this.infoLabel = game.add.group();
        this.lifeValLabel = game.add.text(this.pos.x, this.pos.y, this.lifeVal, { font: '14px "Lucida Console", Monaco, monospace', fill: '#000000', align: 'left' }, this.infoLabel);
        this.scoreLabel = game.add.text(20, 20, `SCORE ${this.score}`, { font: '3rem Impact, Monaco, monospace', fill: '#353b48', align: 'left' });
        //this.bltCntLabel = game.add.text(this.pos.x, this.pos.y, this.currentBltCnt, { font: '14px "Lucida Console", Monaco, monospace', fill: 'orange', align: 'left' }, this.infoLabel);

        // relationship
        this.collider = [];

        //weapon
        this.enableAntiWeapon = false;
        this.antiWeaponCnt = 0;
        this.enableHeavyMechine = false;
        this.heavyMachineCnt = 0;

        this.emitter = game.add.emitter(obj.x, obj.y, 15);
        this.emitter.makeParticles('player');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(1, 0, 1, 0, 500);

        this.mgSnd = obj.mgSnd || null;
    }
    get size() { return this.r; }
    get position() { return this.pos; }
    get sprite() { return this.player; }

    set lifeValCal(num) { this.lifeVal += num; }
    set scoreCal(num) { this.score += num;}

    addCollide(_obj) {
        if (!_obj) this.error('collider undefined');
        if (this.collider.length < _obj.obj.length) {
            _obj.obj.forEach(item => { this.collider.push(item) });
        }
        this.collider.forEach(item => {
            if (game.physics.arcade.collide(this.player, item.sprite)) {
                if (item.callback) item.callback();
            }
        });
    }
    removeCollider(idx) {
        this.collider.splice(idx, 1);//this.collider.filter(cldr => { return })
    }

    movePlayer() {

        if (this.lifeVal < 0 || !this.player.inWorld) return true;
        //POS('palyer', this.player);
        // POS('palyer2', this.pos);
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -this.velocity;
            this.player.facingLeft = true;
            this.player.animations.play('walk'); 
        }
        else if (this.cursor.right.isDown) {
            this.player.body.velocity.x = this.velocity;
            this.player.facingLeft = false;
            this.player.animations.play('walk'); 
        }

        // If the up arrow key is pressed, And the player is on the ground.
        else if (this.cursor.up.isDown) {
            this.player.body.velocity.y = -this.velocity;
            if (this.player.body.touching.down) {
                // Move the player upward
                if (this.player.facingLeft) {

                } else {

                }
            }
            this.player.animations.play('walk'); 
        }
        else if (this.cursor.down.isDown) {
            this.player.body.velocity.y = this.velocity;
            this.player.animations.play('walk'); 
        }
        /*else if (mainState._keyboard) {
            this.shoot();
        }*/
        // If neither the right or left arrow key is pressed
        else {
            // Stop the player 
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
            this.player.animations.stop(); 
            this.player.frame = 0; 

            if (this.player.facingLeft) {

            } else {

            }

        }
        this.pos.x = this.player.x + this.r / 2;
        this.pos.y = this.player.y + this.r / 2;

        this.infoLabel.forEach(item => {
            item.x = this.pos.x;// - idx*this.r;
            item.y = this.pos.y - this.r / 4;
        })

        this.lifeValLabel.text = this.lifeVal;

        for (let i = 0; i < this.bulletSet.length; i++) {

            if (this.bulletSet[i].bullet) this.bulletSet[i].shoot();
            else this.bulletSet.splice(i, 1);

            if (this.bulletSet[i] && !this.detect(this.bulletSet[i].position, this.detectRange).nearby) {
                this.bulletSet.splice(i, 1);
            }
        }

        this.scoreUpdate();
        return false;
    }

    detect(pos = { x: this.pos.x, y: 0 }, cmp = Infinity) {
        var dst = Math.sqrt((pos.x - this.pos.x) * (pos.x - this.pos.x) + (pos.y - this.pos.y) * (pos.y - this.pos.y));

        return {
            distance: dst,
            nearby: (dst < cmp)
        }
    }

    shoot(opt) {

        if (this.bulletSet.length < this.MaxBulletCount) {
            var is_normal_blt = !this.enableHeavyMechine && !this.enableAntiWeapon;
            var num = ((this.enableHeavyMechine) ? 5 : 1);
            var _tx = (false) ?
                this.collider[0].position.x : this.pos.x;
            var _ty = (false) ?
                this.collider[0].position.y : 0;
            for (let i = 0; i < num; i++) {
                var newBullet = new Bullet({
                    x: this.pos.x,
                    y: this.pos.y,
                    tx: _tx + ((this.enableHeavyMechine) ? 1000 * Math.cos((60 + 15 * i) * Math.PI / 180) : 0),
                    ty: _ty + ((this.enableHeavyMechine) ? this.pos.y - 1000 * Math.sin((60 + 15 * i) * Math.PI / 180) : 0),
                    player: this.collider,
                    power: this.power,
                    missilize: (this.enableAntiWeapon && this.collider[0])
                });
                this.bulletSet.push(newBullet);
            }
            //if (!is_normal_blt) this.bulletSet.push(newBullet);
            if (!is_normal_blt) {
                if (this.enableHeavyMechine || this.mgSnd) this.mgSnd.play();
                if (this.enableHeavyMechine && !--this.heavyMachineCnt) {
                    this.enableHeavyMechine = false;
                    this.MaxBulletCount = this.enableAntiWeapon ? Infinity : 1;
                }
                if (this.enableAntiWeapon && !--this.antiWeaponCnt) {
                    this.enableAntiWeapon = false;
                    this.MaxBulletCount = this.enableHeavyMechine ? 1 : 5;
                }
            }
        }
    }
    
    remove() {
        while (this.collider.length) this.collider.pop();
        while (this.bulletSet.length) this.bulletSet.pop();
        this.player.destroy();
        this.player = null;
    }

    scoreUpdate() {
        this.scoreLabel.text = `SCORE ${this.score}`;
    }

    error(msg) {
        console.error('[ PLAYER ] ' + msg);
        return;
    }
}