﻿class EnemyA extends Enemy {
    constructor(obj) {
        super(obj);
        this.MaxBulletCount = 1;
    }

    moveEnemy() {
        if (this.lifeVal < 0) return true;
        //console.log(this.target[0], this.target.length)
        var rge = this.detect(this.target[0].pos);
        if (rge.nearby) this.attack();

        var t_pos = this.target[0].position;

        var dx = (t_pos.x - this.pos.x);
        var dy = (t_pos.y - this.pos.y);
        var slide = Math.sqrt(dx * dx + dy * dy);
        var vx = Math.round(dx / slide * this.velocity);//-Math.round(Math.acos(dx / slide) * this.velocity);
        var vy = Math.round(dy / slide * this.velocity);//-Math.round(Math.asin(dy / slide) * this.velocity); //*15 / this.velocity
        this.enemy.body.velocity.x = vx;
        this.enemy.body.velocity.y = vy;
        this.pos.x = this.enemy.x + this.r;
        this.pos.y = this.enemy.y + this.r;

        this.lifeValLabel.x = this.pos.x;
        this.lifeValLabel.y = this.pos.y - this.r / 4;
        this.lifeValLabel.text = this.lifeVal;
        return false;
    }

    attack() {
        var target_pos = this.target[0].position;
        var pos = this.pos;

        if (this.bulletSet.length < this.MaxBulletCount) {
            var newBullet = new Bullet({
                x: pos.x,
                y: pos.y,
                tx: target_pos.x,
                ty: target_pos.y,
                player: this.target
            });

            this.bulletSet.push(newBullet);
        }
        for (let i = 0; i < this.bulletSet.length; i++) {

            if (this.bulletSet[i].bullet) this.bulletSet[i].shoot();
            else this.bulletSet.splice(i, 1);

            if (this.bulletSet[i] && !this.detect(this.bulletSet[i].position, 300).nearby) {
                this.bulletSet.splice(i, 1);
            }
        }
    }
}
class EnemyB extends Enemy {
    constructor(obj) {
        super(obj);
        this.MaxBulletCount = 1;
        this.bound = { left: obj.left || -50, right: obj.right || (game.width +50) };
        this.isVertical = obj.isVertical || false;
        this.detectRange = 800;
    }

    moveEnemy() {
        if (this.lifeVal < 0 || this.enemy.y > game.height) return true;
        //console.log(this.target[0], this.target.length)
        var rge = this.detect(this.target[0].pos);
        if (rge.nearby) this.attack();

        if (!this.isVertical) {
            if (this.enemy.x <= this.bound.left || this.enemy.x >= this.bound.right) {
                this.velocity = this.velocity * -1;
            }
        }
        if (this.isVertical) {
            if (this.enemy.y <= this.bound.left || this.enemy.y >= this.bound.right) {
                this.velocity = this.velocity * -1;
            }
        }
        this.enemy.x += (!this.isVertical)? this.velocity : 0;
        this.enemy.y += (this.isVertical) ? this.velocity : 0;
        this.pos.x = this.enemy.x + this.r;
        this.pos.y = this.enemy.y + this.r;

        this.lifeValLabel.x = this.pos.x;
        this.lifeValLabel.y = this.pos.y - this.r / 4;
        this.lifeValLabel.text = this.lifeVal;
        return false;
    }

    attack() {
        var pos = this.pos;

        if (this.bulletSet.length < this.MaxBulletCount) {
            var newBullet = new Bullet({
                x: pos.x,
                y: pos.y,
                tx: pos.x,
                ty: game.height,
                player: this.target
            });

            this.bulletSet.push(newBullet);
        }
        for (let i = 0; i < this.bulletSet.length; i++) {

            if (this.bulletSet[i].bullet) this.bulletSet[i].shoot();
            else this.bulletSet.splice(i, 1);

            if (this.bulletSet[i] && !this.detect(this.bulletSet[i].position, 300).nearby) {
                this.bulletSet.splice(i, 1);
            }
        }
    }
}
class EnemyC extends Enemy {
    constructor(obj) {
        super(obj);
        this.MaxBulletCount = 1;
        this.power = 3;
        this.lifeVal = 500;
        this.detectRange = Infinity;
        this.attackRange = 500;
        this.MaxBulletCount = 20;
    }

    moveEnemy() {
        if (this.lifeVal < 0 || this.enemy.y > game.height) return true;
        //console.log(this.target[0], this.target.length)
        var rge = this.detect(this.target[0].pos, this.detectRange);
        if (rge.nearby) this.attack();

        var t_pos = this.target[0].position;

        var dx = (t_pos.x - this.pos.x);
        var dy = (t_pos.y - this.pos.y);
        var slide = Math.sqrt(dx * dx + dy * dy);
        var vx = Math.round(dx / slide * this.velocity);//-Math.round(Math.acos(dx / slide) * this.velocity);
        var vy = Math.round(dy / slide * this.velocity);//-Math.round(Math.asin(dy / slide) * this.velocity); //*15 / this.velocity
        this.enemy.body.velocity.x = vx;
        this.enemy.body.velocity.y = vy;
        this.pos.x = this.enemy.x + this.r;
        this.pos.y = this.enemy.y + this.r;

        this.lifeValLabel.x = this.pos.x;
        this.lifeValLabel.y = this.pos.y - this.r / 4;
        this.lifeValLabel.text = this.lifeVal;
        return false;
    }

    attack() {
        var target_pos = this.target[0].position;
        var pos = this.pos;

        if (this.bulletSet.length < this.MaxBulletCount) {
            for (let i = -5; i < 5; i+=2) {
                var newBullet = new Bullet({
                    x: this.pos.x,
                    y: this.pos.y,
                    tx: this.pos.x +250 * Math.cos((60 * i) * Math.PI / 180),
                    ty: this.pos.y  -250  * Math.sin((28 * i) * Math.PI / 180),
                    player: this.target,
                    power: this.power,
                    velocity: 150 +Math.abs(i)*50,
                    missilize: true
                });
                this.bulletSet.push(newBullet);
            }
        }
        for (let i = 0; i < this.bulletSet.length; i++) {

            if (this.bulletSet[i].bullet) this.bulletSet[i].shoot();
            else this.bulletSet.splice(i, 1);

            if (this.bulletSet[i] && !this.detect(this.bulletSet[i].position, this.attackRange).nearby) {
                this.bulletSet.splice(i, 1);
            }
        }
    }
}