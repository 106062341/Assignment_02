﻿class Bullet {
    constructor(obj) {
        this.bullet = game.add.sprite(obj.x || 0, obj.y || 0, 'bullet');
        this.bullet.anchor.setTo();
        this.r = 8;
        this.pos = {
            x: obj.x || 0,
            y: obj.y || 0
        };
        this.t_pos = {
            x: obj.tx,
            y: obj.ty
        }
        this.velocity = 200;
        this.target = obj.player;
        this.power = obj.power || 1;
        this.missilize = obj.missilize || false;

        game.physics.arcade.enable(this.bullet);

        this.dx = obj.tx - obj.x;
        this.dy = obj.ty - obj.y;
        this.slide = Math.sqrt(this.dx * this.dx + this.dy * this.dy);
        this.vx = Math.round(this.dx / this.slide * this.velocity);
        this.vy = Math.round(this.dy / this.slide * this.velocity);
    }
    get size() { return this.r; }
    get position() { return this.pos; }

    hurt(target) {
        target['_prnt'].lifeValCal = -this.power;
        target['_prnt'].emitter.x = this.pos.x;
        target['_prnt'].emitter.y = this.pos.y;
        target['_prnt'].emitter.start(true, 800, null, 15);
        target.scale.setTo(0.9);
        this.remove();
    }
    removeTarget(idx) {
        this.target.splice(idx, 1);//this.collider.filter(cldr => { return })
    }

    shoot() {

        if (this.missilize && this.target[0]) {
            var obj = this.target[0].position;
            this.dx = obj.x - this.pos.x;
            this.dy = obj.y - this.pos.y;
            this.slide = Math.sqrt(this.dx * this.dx + this.dy * this.dy);
            var vx = Math.round(this.dx / this.slide * this.velocity);
            var vy = Math.round(this.dy / this.slide * this.velocity);

            this.bullet.body.velocity.x = -this.vx * 2 + vx * 2.5;
            this.bullet.body.velocity.y = -this.vy * 2 + vy * 2.5;
        } else {
            this.bullet.body.velocity.x = this.vx;
            this.bullet.body.velocity.y = this.vy;
        }
        this.pos.x = this.bullet.x + this.r;
        this.pos.y = this.bullet.y + this.r;

        for (let i = 0; i < this.target.length; i++) {
            //if (this.target[i].sprite) {
            this.target[i].sprite['_prnt'] = this.target[i];
            game.physics.arcade.overlap(this.target[i].sprite, this.bullet, this.hurt, null, this);
            //}
        }

        if (this.bullet && !this.bullet.inWorld) {
            this.remove();
        }
    }

    remove() {
        this.bullet.destroy();
        this.bullet = null;
    }
}