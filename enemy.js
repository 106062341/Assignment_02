﻿class Enemy {
    constructor(obj) {
        this.enemy = game.add.sprite(obj.x - 12 || -12, obj.y - 12 || -12, 'enemy');
        this.r = obj.r || 25;
        this.R = obj.r || 25;
        this.target = obj.target;
        this.pos = {
            x: obj.x || 0,
            y: obj.y || 0
        };
        this.velocity = obj.v || 80;
        this.lifeVal = obj.r * 2 || 50;
        this._life_val_ = obj.r * 2 || 50;
        this.lifeValLabel = game.add.text(this.pos.x, this.pos.y, this.lifeVal, { font: '14px "Lucida Console", Monaco, monospace', fill: '#000000', align: 'left' });
        this.attackRange = obj.attackRange || 300;
        this.detectRange = obj.detectRange || 300;
        game.physics.arcade.enable(this.enemy);

        //
        this.bulletSet = [];
        this.bullet_counter = 0;
        this.collider = [];

        this.emitter = game.add.emitter(obj.x, obj.y, 15);
        this.emitter.makeParticles('enemy');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(1, 0, 1, 0, 500);

        this.expSnd = obj.expSnd || null;
    }
    get size() { return this.r; }
    get position() { return this.pos; }
    get sprite() { return this.enemy; }

    set lifeValCal(num) { this.lifeVal += num; }

    addCollide(_obj) {
        if (!_obj) this.error('collider undefined');
        if (this.collider.length < _obj.obj.length) {
            _obj.obj.forEach(item => { this.collider.push(item) });
        }
        this.collider.forEach(item => {
            if (game.physics.arcade.collide(this.enemy, item.sprite)) {
                if (item.callback) item.callback();
            }
        });
    }

    removeCollider(idx) {
        this.collider.splice(idx, 1);//this.collider.filter(cldr => { return })
    }

    detect(pos = this.target[0].pos, cmp = this.detectRange) {
        var dst = Math.sqrt((pos.x - this.pos.x) * (pos.x - this.pos.x) + (pos.y - this.pos.y) * (pos.y - this.pos.y));

        return {
            distance: dst,
            nearby: (dst < cmp)
        }
    }

    remove() {
        while (this.bulletSet.length) this.bulletSet.pop();
        while (this.collider.length) this.collider.pop();
        this.lifeValLabel.destroy();
        this.enemy.destroy();
        this.enemy = null;

        if (this.expSnd) this.expSnd.play();
    }
}