var CTX;
window.onload = function () {

    var c = document.querySelector("#canvas1");
    CTX = c.getContext("2d");
    CTX.strokeStyle = 'red';

}
var ARC = {};
function POS(name, update) {
    ARC[name] = update || {x:300, y:250};
}

///  ///  ///
const FPS = 100;
var LOG = {}, counter =0;
function REG(name, update) {
    LOG[name] = update || name;
}
function LOGGER() {
    if (counter++ >= FPS) {
        for (let i in LOG) { console.log(i, LOG[i]); }
        for (let i in ARC) {
            CTX.beginPath();
            if (ARC[i].clr) CTX.strokeStyle = ARC[i].clr;
            CTX.arc(ARC[i].x , ARC[i].y, 5, 0, 2 * Math.PI);
            CTX.stroke();
        }
        counter = 0;
    }
}
///  ///  ///

var mainState = {
    preload: function () {

        // background
        game.load.image('background', 'assets/gameBackGround.png');

        // bar
        game.load.spritesheet('lifeVal', 'assets/lifeBar.png');
        game.load.spritesheet('anti-weapon-val', 'assets/powerBar1.png');
        game.load.spritesheet('heavy-mechine-val', 'assets/powerBar2.png');
        game.load.spritesheet('volume-val', 'assets/volumeBar.png', 100, 20);
        game.load.spritesheet('volume-ctr', 'assets/volumeBarCtr.png');

        // cell
        game.load.spritesheet('bullet', 'assets/bullet.png', 16, 16);
        game.load.spritesheet('player', 'assets/red_ball_ani.png', 50, 50);
        game.load.spritesheet('enemy', 'assets/blue_ball.png', 50, 50);
        game.load.spritesheet('heavy-mechine', 'assets/heavyMechine.png', 16, 16);
        game.load.spritesheet('anti-weapon', 'assets/antiWeapon.png', 16, 16);
        game.load.spritesheet('medical', 'assets/medicine.png', 16, 16);

        //border
        game.load.image('borderV', 'assets/borderV.png');
        game.load.image('borderH', 'assets/borderH.png');

        // audio
        game.load.audio('explosion', 'assets/Explosion.wav');
        game.load.audio('mechine-gun', 'assets/MachineGun.wav');

    },
    create: function () {

        //game
        game.stage.backgroundColor = '#fff';
        this.background = game.add.tileSprite(0, 0, 600, 700, "background");
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;

        // wall
        this.walls = game.add.group();
        this.walls.enableBody = true;
        game.add.sprite(0, 0, 'borderV', 0, this.walls);
        game.add.sprite(game.width + 1, 0, 'borderV', 0, this.walls);
        game.add.sprite(0, 0, 'borderH', 0, this.walls);
        game.add.sprite(0, game.height + 1, 'borderH', 0, this.walls);
        this.walls.setAll('body.immovable', true);

        this.cursor = game.input.keyboard.createCursorKeys();

        // sound effect
        this.explosionSound = game.add.audio('explosion');
        this.mechineGunSound = game.add.audio('mechine-gun');

        // init factor
        this._level_ = getLevel();// request level from level.js
        this._mode_ = 'single-player';
        this.currentLvl = 0;
        this.currentStep = 0;
        this.currentStepLen = 0;
        this.currentStepLen = this._level_[this.currentLvl]._level.length;
        this.win = false;

        // bar
        this.lifeBar = new Bar({
            x: 20,
            y: game.height - 50,
            barName: 'lifeVal',
            barText: 'LifeValue',
            val: 100,
            enableScale: true
        });
        this.weaponBarHM = new Bar({
            x: 20,
            y: game.height - 75,
            barName: 'heavy-mechine-val',
            barText: 'HeavyMechine',
            val: 0,
            _val: 100,
            enableScale: true
        });
        this.weaponBarAW = new Bar({
            x: 20,
            y: game.height - 100,
            barName: 'anti-weapon-val',
            barText: 'AntiWeapon',
            val: 0,
            _val: 100,
            enableScale: true
        });
        this.volumeBar = new Bar({
            x: game.width / 2-50,
            y: 50,
            barName: 'volume-val',
            barText: 'volume',
            val: 50,
            _val: 100,
            enableScale: false
        });

        // player
        this.player = new Player({ x: game.width / 2, y: game.height * 3 / 4, r: 50, mgSnd: this.mechineGunSound });
        //this.players[0] = new Player({ x: game.width / 2, y: game.height * 3 / 4, r: 50, mgSnd: this.mechineGunSound });
        //this.players[0] = _player;
        //this.players = this.players;
        //this.players[1] = 2;
        //this.player = game.add.sprite(game.width / 2, game.height / 2, 'player');
        //this.player.facingLeft = false;

        // notifyer
        this.notifyLabel = game.add.text(0, game.height / 2, 'NA', {
            font: '5rem Impact, Monaco, monospace',
            fill: '#353b48',
            align: 'center',
            boundsAlignH: 'center'
        });
        this.notifyLabel.alpha = 0;
        this.notifyLabel.setTextBounds(0, 0, game.width, 50);

        // enemy
        this.enemy = [];
        this.levelHandler();

        // create element
        this.element = [];
        for (let i = 0; i < 5; i++) {
            this.addElement();
        }

        // keyboard setting
        this._keyboard = game.input.keyboard;
        this._keyboard.addKey(Phaser.Keyboard.SPACEBAR).onDown.add(this.player.shoot, this.player);
        this._keyboard.addKey(Phaser.Keyboard.NUMPAD_ADD).onDown.add(this.volumePlus, this);
        this._keyboard.addKey(Phaser.Keyboard.NUMPAD_SUBTRACT).onDown.add(this.volumeMinus, this);
        this._keyboard.addKey(Phaser.Keyboard.P).onDown.add(this.toggleGaming, this);

        // developer
        this._keyboard.addKey(Phaser.Keyboard.F1).onDown.add(() => {
            this.player.enableHeavyMechine = true;
            this.player.heavyMachineCnt = 50;
            this.player.MaxBulletCount = Math.max(5, (this.player.enableAntiWeapon) ? Infinity : 0);
        }, this);
        this._keyboard.addKey(Phaser.Keyboard.F2).onDown.add(() => {
            this.player.enableAntiWeapon = true;
            this.player.antiWeaponCnt = 200;
            this.player.MaxBulletCount = Infinity;
        }, this);
        this._keyboard.addKey(Phaser.Keyboard.F3).onDown.add(() => {
            this.player.lifeVal = 100;
        }, this);

        this.notify(`level ${this.currentLvl + 1}`);

    },
    volumePlus() {
        this.volumeBar.valCal = Math.min(this.volumeBar.val + 1, 100);
        game.backgroundVolume.volume = this.volumeBar.val / 100;
    },
    volumeMinus() {
        this.volumeBar.valCal = Math.max(0, this.volumeBar.val - 1);
        game.backgroundVolume.volume = this.volumeBar.val / 100;
    },
    toggleGaming() {
        console.log(game.paused, game.isRunning);
        if (game.paused) {
            game.paused = !game.paused;
        } else {
            this.notify('PAUSED', function () { game.paused = !game.paused; });
        }
    },
    notify: function (str, clbk) {
        this.notifyLabel.text = str;
        game.add.tween(this.notifyLabel).to({ y: game.height / 2 - 10 }, 800).yoyo(true).start();
        game.add.tween(this.notifyLabel).to({ alpha: 1 }, 800).yoyo(true).easing(Phaser.Easing.Bounce.Out).start();
        //game.add.tween(this.notifyLabel).to({ alpha: 1, y: game.height / 2 - 10 }, 1000).yoyo(true).easing(Phaser.Easing.Bounce.Out).start();
        if (typeof clbk =='function') window.setTimeout(clbk, 600);
    },
    addElement: function () {
        var rdm = Math.floor(Math.random() * 100);
        if (rdm > 90) {
            this.element.push(new Element({
                x: game.world.randomX,
                y: game.world.randomY,
                type: 'anti-weapon',
                power: 3,
                eater: [this.player],
                velocity: 1
            }));
        } else if (rdm > 80) {
            this.element.push(new Element({
                x: game.world.randomX,
                y: game.world.randomY,
                type: 'heavy-mechine',
                power: 3,
                eater: [this.player],
                velocity: 3
            }));
        } else {
            this.element.push(new Element({
                x: game.world.randomX,
                y: game.world.randomY,
                type: 'medical',
                power: 1,
                eater: [this.player]
            }));
        }
    },
    levelHandler: function () {
        for (let enemy in this._level_[this.currentLvl]._level[this.currentStep]) {
            switch (enemy) {
                case 'enmA':
                    this._level_[this.currentLvl]._level[this.currentStep][enemy].forEach(cell => {
                        this.enemy.push(new EnemyA({ ...cell, target: [this.player], expSnd: this.explosionSound }));
                    });
                    break;
                case 'enmB':
                    this._level_[this.currentLvl]._level[this.currentStep][enemy].forEach(cell => {
                        this.enemy.push(new EnemyB({ ...cell, target: [this.player], expSnd: this.explosionSound }));
                    });
                    break;
                case 'enmC':
                    this._level_[this.currentLvl]._level[this.currentStep][enemy].forEach(cell => {
                        this.enemy.push(new EnemyC({ ...cell, target: [this.player], expSnd: this.explosionSound }));
                    });
                    break;
                default: break;
            }
        }
    },
    update: function () {
        this.background.tilePosition.y += 0.5;
        game.physics.arcade.collide(this.player.sprite, this.walls);
        
        if (this.player.movePlayer()) {
            for (let i = 0; i < this.enemy.length; i++) {
                this.enemy[i].removeCollider(0);
                if (i == this.enemy.length) this.player.remove();
            }
            game.state.start('gameOver');
        } else {
            this.player.addCollide({
                obj: this.enemy
            });
            if (this.player.enableAntiWeapon) this.player.shoot();

            this.lifeBar.valCal = this.player.lifeVal;
            this.weaponBarAW.valCal = this.player.heavyMachineCnt;
            this.weaponBarHM.valCal = this.player.antiWeaponCnt;
            this.player.movePlayer();
        }
        
        if (this.enemy.length) {
            for (let i = 0; i < this.enemy.length; i++) {
                this.enemy[i].addCollide({
                    obj: this.enemy
                });
                if (this.enemy[i].moveEnemy()) {
                    this.player.scoreCal = this.enemy[i].R;
                    this.enemy[i].remove();
                    this.player.removeCollider(i);
                    //this.players.forEach(player => player.removeCollider(i));
                    this.enemy.splice(i, 1);
                    i--;
                }
            }
        } else {
            if (this.currentStep < this.currentStepLen-1) {
                this.currentStep++;
                this.levelHandler();
                
            } else {
                this.currentLvl++;
                this.currentStep = 0;
                if (this.currentLvl < this._level_.length) {
                    this.currentStepLen = this._level_[this.currentLvl]._level.length;
                    this.levelHandler();
                    this.notify(`level ${this.currentLvl+1}`);
                }
                else {
                    this.win = true;
                    game.state.start('gameOver');
                }
            }
        }

        for (let i = 0; i < this.element.length; i++) {
            if (this.element[i].moveElement()) {
                this.element[i].remove();
                this.element.splice(i, 1);
                this.addElement();
            }
        }
        LOGGER();

    }, 
    playerDie: function() { game.state.start('main');},
    
};
var game = new Phaser.Game(600, 700, Phaser.AUTO, 'canvas');
game.state.add('main', mainState);
game.state.add('menu', menuState);
game.state.add('gameOver', gameOverState);
game.state.add('setting', settingState);
game.state.start('menu');