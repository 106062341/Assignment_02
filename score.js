﻿window.onload = function () {
    //requestScoreBoard('single-player');
    
}
function requestScoreBoard(type) {
    var folder = 'singlePlayer';
    switch (type) {
        case 'single-player': folder = 'single-player'; break;
        default: break;
    }

    document.querySelector('#scoreboard small').textContent = type.replace(/-/g, ' ');
    var arr = [];
    firebase.database().ref('cellWar/' + folder).orderByChild('userscore').limitToLast(5).once('value', function (snapshot) {
        arr = Object.entries(snapshot.val());
    }).then(() => {
        var unicode = ['\f091', '\f5a2', '\f559'];
        arr.sort((a, b) => {
            return b[1]['userscore'] - a[1]['userscore'];
        });
        var table = document.querySelectorAll('#scoreboard span'), idx = 0;
        console.log();
        for (let idx = 0; idx < 10; idx += 2) {
            //table[idx].textContent = ((idx / 2 < 3) ? unicode[idx / 2] : `${(idx + 2) / 2 + '. '}`) + arr[idx / 2][1]['username'];
            table[idx].textContent = `${(idx + 2) / 2 + '. '}` + arr[idx / 2][1]['username'];
            table[idx + 1].textContent = arr[idx / 2][1]['userscore'];
        }
    });
}
function postScore(clbk) {
    if (!document.querySelector('#msk input').value.trim()) {
        document.querySelector('#msk span').textContent = mainState.player.score;
        document.querySelector('#msk').style.display = 'flex';
    } else {
        sendScore(clbk);
    }
}
function sendScore(clbk) {
    var newScore = {};

    var newRef = firebase.database().ref('cellWar/' + mainState._mode_).push();
    var key = newRef.key;

    newItem = {
        username: document.querySelector('#msk input').value,
        userscore: mainState.player.score,
        id: key
    }

    newRef.set(newItem);
    document.querySelector('#msk').style.display = 'none';
    document.querySelector('#msk input').value = '';
    if(clbk) clbk();
}