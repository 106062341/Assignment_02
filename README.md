# Software Studio 2019 Spring Assignment 2

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|N|
|Basic rules|20%|N|
|Jucify mechanisms|15%|N|
|Animations|10%|N|
|Particle Systems|10%|N|
|UI|5%|N|
|Sound effects|5%|N|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : 按F1F2作弊可取得兩種武器，吃畫面上的豆也會
2. Animations : 角色移動
3. Particle Systems : 攻擊敵人時會有
4. Sound effects : 背景音+子彈生效
5. Leaderboard : 遊戲畫面左上方

# Bonus Functions Description : 
1. [xxxx] : [xxxx]
2. [xxxx] : [xxxx]
3. [xxxx] : [xxxx]
